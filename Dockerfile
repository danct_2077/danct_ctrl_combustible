# Creando una imagen base de python version slim.
FROM python:3.10.4-slim-bullseye

# Setear variable de entorno, 
# Deshabilitando chequeo de actualizacion de version pip.
ENV PIP_DISABLE_PIP_VERSION_CHECK 1 
ENV PYTHONDONTWRITEBYTECODE 1
# Desactivar almacenamiento en Buffer.
ENV PYTHONNUNBUFFERED 1

# Setear directorio de trabajo
WORKDIR /code

# Instalar dependencias
COPY ./requirements.txt .
RUN pip install -r requirements.txt

# Copiar el proyecto
# Copia todo el directorio actual al directorio de trabajo docker.
COPY . .

